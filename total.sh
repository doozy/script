logger(){
	local action=$1
	if [ $? -eq 0 ]
		then
		echo "Installed $action" >> log.txt
	else
		echo "Error installing $action" >> log.txt
	fi

	return $TRUE
}


# greeting the user
# echo -e "\e[1;32m Welcome to the install guide shell program \e[0m"
# echo " "


# # installing python software properties
# echo -e "\e[1;32m Installing python software properties \e[0m"
# eval 'sudo apt-get install python-software-propertie'
# echo " "

# logger "python-software-properties"

# # install curl
# echo -e "\e[1;32m Installing curl \e[0m"
# eval 'sudo apt-get install curl'
# echo " "

# logger "curl"

# # install git 
# echo -e "\e[1;32m Installing git \e[0m"
# eval 'sudo apt-get install git'
# echo " "

# logger "git"

# # install vim 
# echo -e "\e[1;32m Installing vim \e[0m"
# eval 'sudo apt-get install vim'
# echo " "

# logger "vim"

# # install ssh
# echo -e "\e[1;32m Installing ssh \e[0m"
# eval 'sudo apt-get install openssh-server'
# echo " "

# logger "openssh-server"

# #configure ssh
# echo -e "\e[1;32m Configuring ssh: \e[0m"

# # get the username
# echo -e "\e[1;32m  Enter a name of the user for the ssh: like user@example.com \e[0m"
# read sshuser

# # get the username until it is not nil
# while [ -z $sshuser ] 
# do
#         echo -e "\e[1;31m Enter a name of the user for the ssh: like user@example.com \e[0m"
#         read sshuser
# done

# echo " "
# echo -e "\e[1;32m  generating ssh key \e[0m"
# eval "ssh-keygen -t rsa -C $sshuser"

# logger "generated ssh key"

# echo " "
# #install rvm
# echo -e "\e[1;32m Installing rvm \e[0m"
# eval "\curl -L https://get.rvm.io | bash"
# eval "rvm reload"

# logger "rvm"

# #display the list of rubies available to the user
# eval "rvm list known"
# echo " "
# echo -e "\e[1;32m Choose a ruby version to install \e[0m"
# read rbver

# # try installing ruby
# eval "rvm install $rbver"

# # try installing ruby until success
# while [ $? -ne 0 ]
# do
# 	echo " "
# 	echo -e "\e[1;31m Choose a correct ruby version to install \e[0m"
# 	read rbver
# 	eval "rvm install $rbver"
# done

# logger "ruby"

# #change gemset to global
# echo " "
# echo -e "\e[1;32m Changing gemset to global \e[0m"
# echo " " 

# #reload rvm so that it works properly
# eval "rvm reload"
# eval "rvm gemset use global"

# #install mysql library
# echo -e "\e[1;32m Installing mysql2 libraries \e[0m"
# eval "sudo apt-get install libmysqlclient-dev"

# logger "libmysqlclient-dev"

# #install rmagick library (used generally)
# echo -e  "\e[1;32m Installing rmagick libraries \e[0m"
# eval "sudo apt-get install libmagickwand-dev"

# logger "libmagickwand-dev"

# #install imagemagick
# echo -e  "\e[1;32m Installing imagemagick \e[0m"
# eval "sudo apt-get install imagemagick"
# echo " "

# logger "imagemagick"

# #ask the user rails version to install
# echo -e "\e[1;32m Which version of rails? Enter d for default \e[0m"
# read railsver

# #install rails
# if [ $railsver = "d" ]
# then
# 	eval "gem install rails"
# else
# 	eval "gem install rails -v $railsver"
# fi

# #install rails untill success
# while [ $? -ne 0 ] 
# do
# 	echo " "
# 	echo -e  "\e[1;31m Provide the correct version of rails. Enter d for default \e[0m"
# 	read railsver1	
# 	if [ $railsver1 = "d" ]
# 	then
#         	eval "gem install rails "
# 	else
#         	eval "gem install rails -v $railsver "
# 	fi
# done

# logger "rails"

#auto install mysql server
# echo " "
# eval 'DEBIAN_FRONTEND=noninteractive' #change it to dialog for it to be interactive
# eval 'echo mysql-server mysql-server/root_password password 18dxstreet | sudo debconf-set-selections'
# eval 'echo mysql-server mysql-server/root_password_again password 18dxstreet | sudo debconf-set-selections'

# #installing mysql server
# echo -e  "\e[1;32m Installing mysql server \e[0m"
# eval 'sudo apt-get install mysql-server'

# logger "mysql server"

echo export DB_ROOT_USER=root >> ~/.bashrc
echo export DB_ROOT_PASSWORD=18dxstreet >> ~/.bashrc

eval 'source ~/.bashrc'

# # ASK user for the user name 
# echo "Enter a username for the database"
# read db_user

# while [ -z $db_user ]
# do
# 	echo "Enter a username for the database"
# 	read db_user
# done

# # ASK user for the user name 
# echo "Enter a password for the database"
# read db_password

# while [ -z $db_password ]
# do
# 	echo "Enter a username for the database"
# 	read db_password
# done

# echo "Enter a project name"
# read db_project_name

# while [ -z $db_project_name ]
# do
# 	echo "Enter a username for the database"
# 	read db_project_name
# done

userlist=`mysql --user=$DB_ROOT_USER --password=$DB_ROOT_PASSWORD -e "Select User from mysql.user;"`

for word in $userlist 
do
	echo $word
done

echo $userlist